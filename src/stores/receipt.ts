import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

import { userAuthStore } from "./auth";
import { useMemberStore } from "./member";


import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'

export const useReceiptStore = defineStore('receipt', () => {
    const authStroe =userAuthStore()
    const memberStroe = useMemberStore()
    const receiptDialog = ref(false)
    const receipt =ref<Receipt>({
        id: 0,
        createDate: new Date(),
        totalBefor: 0,
        total: 0,
        receivedAmount: 0,
        paymentType: "cash",
        userId: authStroe.currentUser.id,
        user: authStroe.currentUser,
        memberId: 0,
        memberDiscount: 0
    })
    
  const receiptItems = ref<ReceiptItem[]>([])
  function addReceiptItem(product: Product) {
      const index = receiptItems.value.findIndex((item) => item.product?.id == product.id)
      if (index >= 0) {
          receiptItems.value[index].unit++
          calReceipt()
      } else {
          const newReceipt: ReceiptItem = {
              id: -1,
              name: product.name,
              price: product.price,
              unit: 1,
              productID: product.id,
              product: product
          }
          receiptItems.value.push(newReceipt)
          calReceipt()
      }
  }
  function removeReceiptItem(receiptItem: ReceiptItem) {
      const index = receiptItems.value.findIndex((item) => item === receiptItem)
      receiptItems.value.splice(index, 1)
      calReceipt()
  }
  function inc(item: ReceiptItem) {
      item.unit++
      calReceipt()
  }
  function dnc(item: ReceiptItem) {
      if (item.unit === 1) {
          removeReceiptItem(item)
      }
      item.unit--
      calReceipt()
  }
  function calReceipt() {
        let totalBefore =0
        for(const item of receiptItems.value) {
            totalBefore = totalBefore+(item.price*item.unit)
    }
    if(memberStroe.currentMember)
    receipt.value.totalBefor = totalBefore *0.95
    else{ receipt.value.totalBefor = totalBefore}

  }
  function showReceiptDialog() {
    receiptDialog.value=true
    receipt.value.receiptItems = receiptItems.value

}
function clear() {
    receiptItems.value = []
    receipt.value = {
        
            id: 0,
            createDate: new Date(),
            totalBefor: 0,
            total: 0,
            receivedAmount: 0,
            paymentType: "cash",
            userId: authStroe.currentUser.id,
            user: authStroe.currentUser,
            memberId: 0,
            memberDiscount: 0

    }
    memberStroe.clear()
}


  return { addReceiptItem,removeReceiptItem,inc,dnc,calReceipt,showReceiptDialog,clear,receiptItems,receipt,receiptDialog }
})
