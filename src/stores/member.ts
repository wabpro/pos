import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
    const members = ref<Member[]>([
        { id: 1, name: 'มานะ นะ', tel: '0956171096' },
        { id: 2, name: 'มานี นิ', tel: '0956171097' },
    ])
    const currentMember = ref<Member|null >()
    const searchMember = (tel: string) => {
        const index = members.value.findIndex((item) => item.tel === tel)
        if (index < 0) {
            currentMember.value= null
        }
        currentMember.value =members.value[index]
    }
    function clear() {
        currentMember.value=null
    }

    return {
        members, currentMember,
        searchMember,clear
    }
})
