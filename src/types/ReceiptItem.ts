import type { Product } from "./Product";

type ReceiptItem = {
    id: number;
    name: string;
    price: number;
    unit: number;
    productID: number;
    product?: Product
}

export {type ReceiptItem}